import { Time } from './time'

export class Torcedor{
    private time: Time;
    private nome: string;
    private idade: number;
    private ingresso: number;
    private musica: string;

    public comprarIngresso(qtd: number): number{
        if (this.ingresso == 0){
            this.ingresso += qtd;
            return this.ingresso;
        }else{
            return 0;
        }
    }
    
    public getIngresso(): number{
        return this.ingresso;
    }

    public setMusica(musica: string){
        this.musica = musica;
    }
}